using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Cips.Application.Common.Interfaces;
using Cips.Application.Events;

namespace Cips.Infrastructure.Services
{
    public class SnsRequestMessageBody
    {
        [JsonPropertyName("default")]
        public string Default  { get; set; }
        
        [JsonPropertyName("json")]
        public string Json  { get; set; }
    }
    
    public class EventBusSns : IEventBus
    {
        private readonly IConfiguration _configuration;

        public EventBusSns(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Task Publish(EventBase domainEvent)
        {
            var message = domainEvent.Serialize();
            var messageBody = new SnsRequestMessageBody
            {
                Default = message,
                Json = message
            };
            var reqBody = JsonSerializer.Serialize(messageBody);

            var pub = new PublishRequest
            {
                TopicArn = _configuration["SNS:TopicArn"],
                Message = reqBody,
                MessageStructure = "json"
            };

            var sns = new AmazonSimpleNotificationServiceClient();
            return sns.PublishAsync(pub);
        }
    }
}